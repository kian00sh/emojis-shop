
FROM node:buster-slim

WORKDIR /app
COPY package.json /app/
COPY server/ /app/server/
RUN yarn
COPY front/ /app/front/
WORKDIR /app/front
RUN yarn
RUN yarn run build
WORKDIR /app
COPY docker-entrypoint.sh /app/
# Make Entrypoint executable
RUN chmod +x docker-entrypoint.sh
# Run the app when the container launches
CMD ["./docker-entrypoint.sh"]
