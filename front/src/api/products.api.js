import ServiceApi from "../services/service.api";

const baseServiceUrl = `/products`;

const ProductsApi = {
  getProducts: ({ _page, _limit = 10, _sort }) => {
    const url = `${baseServiceUrl}/`;
    return ServiceApi.get(url, {
      params: {
        _page,
        _limit,
        _sort,
      },
    });
  },
};

export default ProductsApi;
