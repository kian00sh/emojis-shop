import React, { useEffect, useState, useRef } from "react";
import PropTypes from "prop-types";
import ShopDrawer from "./components/drawer";
import { makeStyles } from "@material-ui/core/styles";
import ProductsApi from "./api/products.api";
import Grid from "@material-ui/core/Grid";
import Product from "./components/product";
import Ads from "./components/ads";
import { CircularProgress, Backdrop } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  mainGrid: {
    marginTop: 50,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

function App() {
  const [products, setProducts] = useState([]);
  const [cachedProducts, setCachedProducts] = useState([]);
  const [_page, setPage] = useState(1);
  const [_sort, setSort] = React.useState("");
  const prevSortRef = useRef();
  const [isBusy, setIsBusy] = useState(true);
  const [scrollExists, setScrollExists] = useState(false);
  const [endReached, setEndReached] = useState(false);
  const [adsCounter, setAdsCounter] = useState(0);

  useEffect(() => {
    document.addEventListener("scroll", trackScrolling);
    prevSortRef.current = _sort;
    fetchProducts();
    return () => document.removeEventListener("scroll", trackScrolling);
  }, [_page, _sort]);

  // Save previous state of sort option
  const prevSort = prevSortRef.current;

  const fetchProducts = async () => {
    try {
      setIsBusy(true);

      let newProducts;

      // Check if we have next batch of product in cache
      if (
        cachedProducts.length &&
        // Check if cached products are duplicate
        !products.find(({ id }) => id === cachedProducts[0].id)
      ) {
        newProducts = cachedProducts;
      } else {
        const { data } = await ProductsApi.getProducts({
          _page,
          _sort,
        });
        newProducts = data;
        // Check if we get to the end of prodct list
        setEndReached(!data.length);
      }
      // Emptying cache
      setCachedProducts([]);

      // we want to make sure we're not adding new products with
      // a sort option to the end of products that have'nt any sort option aplied to them
      if (!_sort || prevSort === _sort) {
        // Adding ads after loading every 20 product
        if (products.length && (products.length - adsCounter) % 20 === 0) {
          setAdsCounter(adsCounter + 1);
          newProducts = [...products, { ad: true }, ...newProducts];
        } else {
          newProducts = [...products, ...newProducts];
        }
      }
      setProducts(newProducts);
    } catch (error) {
      console.log(error);
    } finally {
      setIsBusy(false);
      // Getting next batch of products
      await fetchNextProducts();
    }
  };

  const fetchNextProducts = async () => {
    return new Promise(async (resolve, reject) => {
      try {
        const { data } = await ProductsApi.getProducts({
          _page: _page + 1,
          _sort,
        });
        setCachedProducts(data);
        resolve(data);
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
  };

  const handleChangeSorting = (sort) => {
    setPage(1);
    setCachedProducts([]);
    setProducts([]);
    setScrollExists(false);
    setAdsCounter(0);
    setEndReached(false);
    setSort(sort);
  };

  const trackScrolling = async ({
    target: {
      scrollingElement: { scrollHeight, scrollTop, clientHeight },
    },
  }) => {
    !scrollExists && setScrollExists(true);
    const bottom = scrollHeight - scrollTop === clientHeight;
    if (bottom && !endReached) {
      setPage(_page + 1);
    }
  };

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Backdrop className={classes.backdrop} open={isBusy}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <ShopDrawer handleChangeSorting={handleChangeSorting} _sort={_sort} />
      <main className={classes.content}>
        <Grid
          className={classes.mainGrid}
          container
          direction="row"
          justify="space-evenly"
          alignItems="flex-start"
        >
          {products.map(({ ad, face, date, price, size, id }, index) => {
            if (ad) {
              return <Ads key={index} index={index} />;
            } else {
              return (
                <Product
                  key={id}
                  face={face}
                  date={date}
                  price={price}
                  size={size}
                  id={id}
                />
              );
            }
          })}
        </Grid>
        {endReached && (
          <Grid container direction="column" alignItems="center">
            "~ end of catalogue ~".{" "}
          </Grid>
        )}
        {!scrollExists && (
          <Grid container direction="column" alignItems="center">
            <p
              onClick={() => setPage(_page + 1)}
              style={{ cursor: "pointer", color: "blue" }}
            >
              Load More...
            </p>
          </Grid>
        )}
      </main>
    </div>
  );
}

App.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default App;
