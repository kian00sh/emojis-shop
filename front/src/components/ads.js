import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Paper, Grid, ButtonBase } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
    maxWidth: 500,
    marginBottom: 25,
    marginTop: 25,
  },
  img: {
    margin: "auto",
    display: "block",
    maxWidth: "100%",
    maxHeight: "100%",
  },
}));
function Ads({ index }) {
  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <Grid container spacing={2}>
        <Grid item>
          <ButtonBase className={classes.image}>
            <img
              src={`${process.env.REACT_APP_API_URL}/ads/?r=${index}`}
              alt="alt"
            />
          </ButtonBase>
        </Grid>
      </Grid>
    </Paper>
  );
}

export default Ads;
