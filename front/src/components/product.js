import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { Paper } from "@material-ui/core";
import helper from "../utils/helper";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
    maxWidth: 500,
    marginBottom: 25,
    marginTop: 25,
  },
}));

function Product({ face, date, price, size, id }) {
  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <Grid container spacing={2}>
        <Grid
          container
          direction="column"
          justify="space-evenly"
          alignItems="center"
        >
          <span
            style={{
              fontSize: size,
              maxHeight: 80,
              paddingTop: 80,
              paddingBottom: 80,
              maxWidth: "auto",
            }}
          >
            {face}
          </span>
        </Grid>
        <Grid item xs={12} sm container>
          <Grid item xs container direction="column" spacing={2}>
            <Grid item xs>
              <Typography variant="body2" color="textSecondary">
                ID: {id}
              </Typography>
              <Typography variant="overline">
                {helper.timeDifference(new Date(), new Date(date))}
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Typography variant="subtitle1">${price}</Typography>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  );
}

export default Product;
