# Products Grid

## Setup

### Requirements

- docker
- docker-compose

### How does it work

To start the project:

```sh
$ docker-compose up -d
```

To stop and destroy container:

```sh
$ docker-compose down
```

If you did any changes to code and want to rebuild the docker image:

```sh
$ docker-compose up -d --build
```

If you don't want to use docker:

```sh
$ yarn
$ cd front
$ yarn
$ yarn run build
$ cd ..
$ yarn start
```

App is available at http://localhost:3000

## Approaches to take care of pre fetching products

### Race Condition Approach

Race condition may happens when user is requesting for next batch of products too quickly and it might fetch products that are already present in state. one solution that I could've think of was to check for duplicate products and then read from cache.
it's all explained in code (`app.js` file) and it's also present in `master` branch.

### Fetch And Render Approach

One other approach was to fetch first batch and second batch of products at first and then try to render products. it's actually works and products will queried from the cache whenever user is requesting for next page. but users can't feel it and still sees the loading because we're fetching all the data that we wants and then render the page.
this approaches code is placed in `fetch-and-render-approach` branch.
